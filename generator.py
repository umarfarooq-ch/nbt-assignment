from faker import Faker

fake = Faker()

import requests
import json

url = "http://localhost:8000/api/v1/measurements/"

headers = {'Content-Type': 'application/json'}

for _ in range(2000):
    temperature = fake.pyfloat(min_value=20, max_value=38)
    rssi = fake.pyint(min_value=0, max_value=45)
    humidity = fake.pyint(min_value=0, max_value=100)
    date = fake.date_time()

    payload = json.dumps({
        "date": int(date.timestamp()),
        "device_id": "D3L2",
        "data": {
            "temperature": temperature,
            "rssi": rssi,
            "humidity": humidity
        }
    })
    requests.request("POST", url, headers=headers, data=payload)
