from django.urls import path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()

router.register(r'devices', views.DeviceAPIView, basename="main_device-router")
router.register(r'measurements', views.MeasurementAPIView, basename="main_measurement-router")
router.register(r'companies', views.CompanyAPIView, basename="main_company-router")

urlpatterns = [
    path('measurements/avg/', views.AVGTemp.as_view()),
    *router.urls,
]
