from django.contrib.postgres.fields import ArrayField
from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)

    class Meta:
        db_table = "company"
        verbose_name = "company"
        verbose_name_plural = "companies"


class Device(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='devices')
    device_id = models.CharField(max_length=255, primary_key=True)
    active = models.BooleanField(default=False, )
    labels = ArrayField(models.CharField(max_length=255, ), )

    class Meta:
        db_table = "device"
        verbose_name = "device"
        verbose_name_plural = "devices"


class Measurement(models.Model):
    date = models.DateTimeField(primary_key=True)
    device_id = models.CharField(max_length=255, default="")
    data = models.JSONField()

    class Meta:
        db_table = "measurement"
        verbose_name = "measurement"
        verbose_name_plural = "measurements"
