from rest_framework import viewsets
from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination

from . import models, serializers


class CompanyAPIView(viewsets.ModelViewSet):
    queryset = models.Company.objects.all()
    serializer_class = serializers.CompanySerializer


class DeviceAPIView(viewsets.ModelViewSet):
    serializer_class = serializers.DeviceSerializer
    filterset_fields = ['device_id', 'active', 'company']

    def get_queryset(self, *args, **kwargs):
        devices = models.Device.objects.all()
        labels = self.request.query_params.getlist('labels', None)
        if labels:
            return devices.filter(labels__contains=labels)
        return devices


class MeasurementAPIView(viewsets.ModelViewSet):
    queryset = models.Measurement.objects.all()
    serializer_class = serializers.MeasurementSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        sql = "SELECT * FROM device JOIN measurement ON device.device_id=measurement.device_id "
        where = []
        qp = self.request.query_params.keys()
        for key, value in self.request.query_params.items():
            if key == 'labels':
                values = self.request.query_params.getlist('labels')
                where.append(f"cast(device.labels as varchar(255)[]) @> (ARRAY{[x for x in values]})::varchar(255)[]")
            elif key == 'active':
                where.append(f'active={value}')
            elif key == 'company':
                where.append(f'device.company_id = {value}')
            elif key == "start_date":
                where.append(f'date >= to_timestamp({value})')
            elif key == "end_date":
                where.append(f'date <= to_timestamp({value})')

        # parse where, generate full sql statement
        if where:
            full_sql = f"{sql} WHERE "
            for x in where:
                full_sql += f"{x} and "

            # delete last and in where clause
            full_sql = full_sql[:-4]
            return models.Measurement.objects.raw(full_sql)
        return models.Measurement.objects.raw(sql)


class AVGTemp(ListAPIView):
    serializer_class = serializers.AVGSerializer

    def get_queryset(self):
        sql = "SELECT to_timestamp(1) as date, AVG((data->>'temperature')::float)::float  as avg_temperature FROM " \
              "device JOIN measurement ON device.device_id=measurement.device_id "
        where = []
        qp = self.request.query_params.keys()
        for key, value in self.request.query_params.items():
            if key == 'labels':
                values = self.request.query_params.getlist('labels')
                where.append(f"cast(device.labels as varchar(255)[]) @> (ARRAY{[x for x in values]})::varchar(255)[]")
            elif key == 'active':
                where.append(f'active={value}')
            elif key == 'company':
                where.append(f'device.company_id = {value}')
            elif key == "start_date":
                where.append(f'date >= to_timestamp({value})')
            elif key == "end_date":
                where.append(f'date <= to_timestamp({value})')

        # parse where, generate full sql statement
        if where:
            full_sql = f"{sql} WHERE "
            for x in where:
                full_sql += f"{x} and "

            # delete last and in where clause
            full_sql = full_sql[:-4]
            return models.Measurement.objects.raw(full_sql)
        return models.Measurement.objects.raw(sql)
