# Generated by Django 4.1.6 on 2023-02-08 09:00

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0002_addded_measurement'),
    ]

    operations = [
        migrations.RunSQL(
            """
            CREATE EXTENSION IF NOT EXISTS postgres_fdw;
            CREATE SERVER db2_server FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'timescale', dbname 'timescale',
            port '5433');
            CREATE FOREIGN TABLE measurement (
                date TIMESTAMP,
                device_id CHAR(255) DEFAULT '',
                data JSONB
            ) SERVER db2_server OPTIONS (table_name
            'measurement');
            CREATE USER MAPPING
            FOR PUBLIC
            SERVER db2_server
            OPTIONS (user 'postgres', password 'postgres');

            """,
            hints={"for": "default"},

        ),
        migrations.RunSQL(
            """
            CREATE EXTENSION IF NOT EXISTS postgres_fdw;
            CREATE SERVER db1_server FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'db', dbname 'postgres',
            port '5432');
            CREATE FOREIGN TABLE device (
                company_id INTEGER,
                device_id CHAR(255),
                active BOOLEAN DEFAULT FALSE,
                labels CHAR(255)[]
            ) SERVER db1_server OPTIONS (table_name
            'device');
            CREATE USER MAPPING
            FOR PUBLIC
            SERVER db1_server
            OPTIONS (user 'postgres', password 'postgres');

            """,
            hints={"for": "measurement"},

        ),

    ]
