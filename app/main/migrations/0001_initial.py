# Generated by Django 4.1.6 on 2023-02-08 08:32

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'company',
                'verbose_name_plural': 'companies',
                'db_table': 'company',
            },
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('device_id', models.CharField(max_length=255, primary_key=True, serialize=False)),
                ('active', models.BooleanField(default=False)),
                ('labels', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=255), size=None)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='devices', to='main.company')),
            ],
            options={
                'verbose_name': 'device',
                'verbose_name_plural': 'devices',
                'db_table': 'device',
            },
        ),
    ]
