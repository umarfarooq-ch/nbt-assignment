from datetime import datetime

from rest_framework import serializers

from . import models


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Company
        fields = "__all__"


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Device
        fields = '__all__'


class UnixTimestampField(serializers.Field):
    """serialize UNIX timestamp into python datetime"""

    def to_representation(self, value):
        return int(value.timestamp())

    def to_internal_value(self, value):
        return datetime.fromtimestamp(int(value))


class MeasurementSerializer(serializers.ModelSerializer):
    date = UnixTimestampField()

    class Meta:
        model = models.Measurement
        fields = "__all__"


class AVGSerializer(serializers.Serializer):
    avg_temperature = serializers.FloatField()
    class Meta:
        model = models.Measurement
        fields = ("avg_temperature", )
