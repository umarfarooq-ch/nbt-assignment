class Router:
    def db_for_read(self, model, **hints):
        if model._meta.db_table == 'measurement':
            return 'timescale'
        return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.db_table == 'measurement':
            return 'timescale'
        return 'default'

    def allow_migrate(self, db, app, model_name=None, **hints):
        if db == "timescale":
            if model_name == "measurement" or hints.get("for") == "measurement":
                return True
            return False
        else:
            if model_name != 'measurement' and model_name is not None:
                return True
            elif hints.get("for") == "default":
                return True
            return False
