# Sample django project

project description

# Installations
Steps to run django application server:
============
## 1. Dependencies:
To be able to run **django server** you have to meet following dependencies:

- python3.10, python3.10-dev, pip3, and pipenv

## 2. Install App Requirements:
- Switch to project root directory.
- Run `$ pip install -r requirements.txt`

## 3. Configurations:
- Copy the `.env` file  in `/$PROJECT_ROOT/app/`

## 4. Apply migrations:
- Switch to project root directory.
- Run `$ python manage.py migrate`

## 5. Start Application Server:
- Switch to project root directory.

- **for development server**
  
  Run `$ python manage.py runserver`


server is now up on `host:8000`

