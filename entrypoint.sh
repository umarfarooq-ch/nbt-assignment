#!/bin/bash

# This script is executed when the Django Docker container is started.
#
# In the beginning the script waits until the database container is fully loaded and ready.
# Only then migrations and initialization scripts can be executed.

echo "Migrate ... "
python3 manage.py migrate --noinput
python3 manage.py migrate --noinput --database=timescale
echo "done"

echo "Collectstatics ... "
python3 manage.py collectstatic --noinput
echo "done"

echo "App initialized ..."


echo "Starting project..."



# The uvicorn ASGI can automatically reload the server in case of a code change.
# This feature can be disabled by removing the "--reload" flag or by using the daphne asgi.
uvicorn --host 0.0.0.0 --port 8000 app.asgi:application