# Django project

Django project with Dockerized deployment, featuring Nginx, PostgreSQL, users app, and PyCharm configurations.

# Installations
Steps to run django application server:
============
## 1. Dependencies:
To be able to run **django server** you have to meet following dependencies:

- docker docker-compose
- libkrb5-dev

## 3. Configurations:
Create `.env` file in `/$PROJECT_ROOT/app/app/` directory

`$ touch /$PROJECT_ROOT/app/app/.env`


Sample .env
```
# Django app configs
DJANGO_SECRET_KEY="SECRET-KEY"
DJANGO_DEBUG=0

# Postgres
POSTGRES_NAME="postgres"
POSTGRES_USER="postgres"
POSTGRES_PASSWORD="postgres"
POSTGRES_HOST="db"
POSTGRES_PORT=5432
```

## 3. Install App Requirements:
- Switch to project root directory.
- Run `$ docker-compose up --build`



django docker application is now up on `https://host`
